<?php


class PromosWidget extends Elementor\Widget_Base
{
    public function get_name()
    {
        return "promos-widget";
    }

    public function get_title()
    {
        return "Promos List";
    }

    public function register_controls()
    {
        $this->start_controls_section(
            'title_section',
            [
                'label' => 'Texts',
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control('title', [
            'label' => 'Title',
            'type' => \Elementor\Controls_Manager::TEXT
        ]);

        $this->end_controls_section();

        $this->start_controls_section(
            'square_section',
            [
                'label' => 'Squares',
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control('size', [
            'label' => 'Size',
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => 100
        ]);

        $this->add_control('squareNb', [
            'label' => 'Amount',
            'type' => \Elementor\Controls_Manager::NUMBER,
            'default' => 2
        ]);

        $this->end_controls_section();
    }

    public function render()
    {

        $settings = $this->get_settings_for_display();

        ?>

        <h3>
            <?= $settings['title'] ?>
        </h3>

        <div style="display: flex; flex-wrap:wrap;">
            <?php for ($x = 0; $x < $settings['squareNb']; $x++) { ?>
                <div style="width: <?= $settings['size'] ?>px; height: <?= $settings['size'] ?>px; background-color: red"></div>
            <?php } ?>
        </div>
        <?php
    }
}