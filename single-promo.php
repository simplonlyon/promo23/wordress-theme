<?php

get_header();

/* Start the Loop */
while ( have_posts() ) :
	the_post();
    ?>
    <h1><?= the_title() ?></h1>
    <p>Référentiel : <?= get_post_meta(get_the_ID(), 'referentiel', true) ?></p>

    <?php
    $query = new WP_Query([
        'post_type' => 'student',
        'meta_query' => [
            [
                'key' => 'promo', 
                'value' => '"' . get_the_ID() . '"',
                'compare' => 'LIKE'
            ]
        ]
    ]);

    while($query->have_posts()):
        $query->the_post();

        the_title();
        $profilePic = get_field('student_picture');
        echo '<img src="'.$profilePic['url'].'" alt="'.$profilePic['alt'].'" width="200" >';


    endwhile;
	
endwhile; // End of the loop.

get_footer();
