<?php


add_action('wp_enqueue_scripts', 'load_styles');
function load_styles()
{
	$parenthandle = 'parent-style';
	$theme = wp_get_theme();
	wp_enqueue_style(
		$parenthandle,
		get_template_directory_uri() . '/style.css',
		[],
		$theme->parent()->get('Version')
	);
	wp_enqueue_style(
		'child-style',
		get_stylesheet_uri(),
		[$parenthandle],
		$theme->get('Version')
	);
}


//Pour rajouter custom post et lui assigner des champs on peut les plugins CPT UI et ACF
add_action('init', 'define_promo');

function define_promo()
{
	register_post_type('promo', [
		'label' => 'Promos',
		'supports' => ['title'],
		'public' => true
	]);

}

add_action('add_meta_boxes_promo', 'promo_meta_boxes');

function promo_meta_boxes()
{

	add_meta_box(
		'promo_fields',
		__('Détail de la promo'),
		'promo_fields',
		'promo',
		'normal',
		'default'
	);

}

function promo_fields(WP_Post $post)
{

	?>
	<div>
		<label for="referentiel">Référentiel</label>
		<input type="text" id="referentiel" name="referentiel" value="<?= get_post_meta($post->ID, 'referentiel', true) ?>">
	</div>
	<div>
		<label for="startDate">Date de début</label>
		<input type="date" id="startDate" name="startDate" value="<?= get_post_meta($post->ID, 'startDate', true) ?>">
	</div>
	<div>
		<label for="endDate">Date de fin</label>
		<input type="date" id="endDate" name="endDate" value="<?= get_post_meta($post->ID, 'endDate', true) ?>">
	</div>

	<?php
}

add_action('save_post_promo', 'save_promo');

function save_promo($post_id)
{
	if (isset($_POST['referentiel'])) {
		update_post_meta($post_id, 'referentiel', esc_html($_POST['referentiel']));
	}

	if (isset($_POST['startDate'])) {
		update_post_meta($post_id, 'startDate', esc_html($_POST['startDate']));
	}
	if (isset($_POST['endDate'])) {
		update_post_meta($post_id, 'endDate', esc_html($_POST['endDate']));
	}
}


add_action('admin_menu', 'define_import_menu');

function define_import_menu()
{
	add_menu_page('Import apprenant·es', 'Import apprenant·es', 'administrator', 'import_menu', 'display_import_menu');
}

function display_import_menu()
{
	?>
	<h1>Importer des apprenant·es</h1>
	<form action="admin-post.php" method="post" enctype="multipart/form-data">
		<label for="csv">Fichier CSV : </label>
		<input type="file" name="csv" id="csv" accept=".csv">
		<input type="hidden" name="action" value="import_student" />

		<button>Importer</button>
	</form>

	<?php
}

add_action('admin_post_import_student', 'handle_import_student');

function handle_import_student()
{

	$rows = array_map('str_getcsv', file($_FILES['csv']['tmp_name']));
	$header = array_shift($rows);
	$csv = array();
	foreach ($rows as $row) {
		$csv[] = array_combine($header, $row);
	}

	foreach ($csv as $line) {

		wp_insert_post([
			'post_title' => $line['Nom'] . ' ' . $line['Prénom'],
			'post_type' => 'student'
		]);
	}


}



function register_oembed_widget( $widgets_manager ) {

	require_once( __DIR__ . '/promos-widget.php' );

	$widgets_manager->register( new \PromosWidget() );

}
add_action( 'elementor/widgets/register', 'register_oembed_widget' );